
# Versions and releases

This uses version numbers that comply with SemVer 2.0.0. Development versions
have a `-dev` suffix.  The version number before a `-dev` is the version that
is under development. A release version number does not have a `-dev` suffix.

In version control, releases and development versions both exist in `master`.
Releases are tagged.

Releases are create as demonstrated below. This is an untested sketch... for
now. Assume current version is 2.0.0-dev. 2.0.0 is released as follows.

```
nvim setup.py     # Change 2.0.0-dev to 2.0.0
git add . && git commit -v 'Release 2.0.0'
git tag v2.0.0
nvim setup.py     # Change 2.0.0 to 2.0.1-dev
git add . && git commit -v 'Bump version 2.0.0 -> 2.0.1-dev'
git push
git push --tags
```

# Updating project requirements

Minimum requirements to run this project are listed in `install_requires`
within `setup.py`.

To add a new requirement for this porject, first add it to `install_requires`
in `setup.py`, and then update `Pipfile` and `Pipfile.lock` as follows.

```
pipenv install '-e .'
pipenv lock
```
