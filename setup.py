from setuptools import setup, find_packages
from os import path

# Read in README.md as the long_description for the project.
here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name = 'GoLLuM',
    version = '2.0.0-dev',
    license = 'GPLv3',
    url = 'https://gitlab.com/kwurst/gitlab-course-managment-scripts.git',

    description = 'Python scripts to manage students and projects in GitLab',
    long_description = long_description,

    author = 'Karl Wurst',
    author_email = 'karl@w-sts.com',

    # Supported platforms (e.g., Windows, MacOS, Linux)
    platforms = [ 'any' ],

    # Manage project requirements as follows
    #   1. Add minimum requirements to install_requires below.
    #   2. Update Pipfile and Pipfile.lock as follows:
    #       pipenv install '-e .'
    #       pipenv lock
    install_requires = [ 'requests' ],

    package_dir = {'':'src'},
    packages = find_packages('src'),

    entry_points = '''
    [console_scripts]
    gllm=gllm.cli:main
    '''
)
