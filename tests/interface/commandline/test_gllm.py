from subprocess import Popen, PIPE


def test_gllm_is_available_from_command_line():
    p = Popen(["gllm", "--help"], stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    assert "usage: gllm" in stdout.decode('utf-8')
